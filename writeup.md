## CarND-Controls-PID
Self-Driving Car Engineer Nanodegree Program

### Compile and Run
You can compile and run the application as described in the README file.

### Description
This program is an implementation of a PID controller tailored for a track in the Udacity simulator. The implementation takes place in the file `main.cpp`. The controller computes the proportional, derivative and integral error in the method `UpdateError()`. Furthermore it computes the total error in the method `TotalError()` by means of coefficients for the three error parts. These coefficients are initialized in the `Init()` method. The negative total error serves as a control input for the steering angle.

### Hyperparameters
The hyperparameters KP, KI and KD were chosen by manual tuning. According to [PID controller](https://en.wikipedia.org/wiki/PID_controller) the hyperparameter for the proportional error term was the first that was chosen. The pathway of the car began to oscilate at a value of 0.3 for KP. Therefore - see again [PID controller](https://en.wikipedia.org/wiki/PID_controller) - a value of 0.15 was determined for the further computation. The effect of KI was very small. A value of 0.0005 set the steady state error to null. Lastly KD was chosen. A value of 5.0 damped the oscillations of the pathway effectivly. The final values KP=0.16, KI=0.0005, KD=6.0 were obtained by finetuning.

